#pragma once

#include "Control.hpp"

class Keyboard : public Control {
public:
	Keyboard(size_t playerNr);

	double getMovement() const override;
	bool getSnake() const override;
	bool getNeutralSnake() const override;
	bool getShoot() const override;
	bool getSwap() const override;
	void draw_help(int playerNr) override;

private:
	const size_t playerNr;
};
