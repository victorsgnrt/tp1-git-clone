#pragma once

#include "engine/Vector2d.hpp"

class Control {
public:
	virtual ~Control() = default;

	/// Bewegung von -1.0 bis 1.0
	virtual double getMovement() const = 0;

	/// Eine neue Schlange spawnen
	virtual bool getSnake() const = 0;

	/// Eine neue Schlange spawnen
	virtual bool getNeutralSnake() const = 0;

	/// Schießen
	virtual bool getShoot() const = 0;

	/// Auf die gegenüberliegende Kante springen
	virtual bool getSwap() const = 0;

	virtual void draw_help(int playerNr) = 0;
};
