#include "Widget.hpp"

Widget::Widget() : sensitive_(true), focus_(false), debug(false) {
}

Widget::~Widget() = default;

bool Widget::getSensitive() const {
	return sensitive_;
}

void Widget::setSensitive(const bool sensitive) {
	sensitive_ = sensitive;
}

void Widget::SetFocus(bool focus) {
	bool old = focus_;
	focus_ = focus;
	if (focus_ != old) {
		OnFocusChanged();
	}
}

void Widget::OnFocusChanged() {
}

void Widget::setDebug(bool d) {
	debug = d;
}

bool Widget::getDebug() const {
	return debug;
}

void Widget::onAdd(Work&) {
}
