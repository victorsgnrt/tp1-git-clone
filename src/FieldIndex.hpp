#pragma once

#include "engine/Vector2d.hpp"

class FieldIndex {
public:
	/// Zeilen-Nummer (y-Koordinate)
	int i;

	/// Spalten-Nummer (x-Koordinate)
	int j;

	/// Rechnet die Bildschirm-Koordinaten in Indexes im Feld um
	static FieldIndex fromScreenPos(Vector2d);

	/// Gibt die entsprechende Bildschirm-Koordinaten zurück
	Vector2d toScreenPos() const;

	FieldIndex& operator+=(const FieldIndex& rhs);
};
constexpr static FieldIndex DIRECTION_UP{ -1, 0 };
constexpr static FieldIndex DIRECTION_DOWN{ 1, 0 };
constexpr static FieldIndex DIRECTION_LEFT{ 0, -1 };
constexpr static FieldIndex DIRECTION_RIGHT{ 0, 1 };
bool operator==(const FieldIndex& lhs, const FieldIndex& rhs);
FieldIndex operator+(const FieldIndex& lhs, const FieldIndex& rhs);
FieldIndex operator-(const FieldIndex& lhs, const FieldIndex& rhs);
