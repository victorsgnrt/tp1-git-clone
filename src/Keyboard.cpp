#include "Keyboard.hpp"
#include "constants.hpp"
#include "engine/Paths.hpp"

#include <jngl.hpp>
#include <jngl/input.hpp>

Keyboard::Keyboard(const size_t playerNr) : playerNr(playerNr) {
}

double Keyboard::getMovement() const {
	if (playerNr == 0) {
		if (jngl::keyDown('s')) { return 1; }
		if (jngl::keyDown('w')) { return -1; }
	} else {
		if (jngl::keyDown(jngl::key::Right)) { return 1; }
		if (jngl::keyDown(jngl::key::Left)) { return -1; }
	}
	return 0;
}

bool Keyboard::getSnake() const {
	return playerNr == 0 ? jngl::keyPressed(jngl::key::Tab)
	                     : (jngl::keyPressed(jngl::key::ControlR) || jngl::keyPressed(jngl::key::AltR));
}

bool Keyboard::getNeutralSnake() const {
	return playerNr == 0 ? jngl::keyPressed('e') : jngl::keyPressed('#');
}

bool Keyboard::getShoot() const {
	return playerNr == 0 ? jngl::keyPressed('q') : jngl::keyPressed('-');
}

bool Keyboard::getSwap() const {
	return playerNr == 0
	           ? (jngl::keyPressed('d') || jngl::keyPressed('a'))
	           : (jngl::keyPressed(jngl::key::Down) || jngl::keyPressed(jngl::key::Up));
}

void Keyboard::draw_help(int playerNr)
{
	const int x = playerNr == 0 ? -900 : 600;
	const int y = 195;
	jngl::print("Controls ", x, y - 10);
	jngl::print(playerNr == 0 ? "Tab : Snake" : "ALT/Strg : Snake", x, y + 32);
	jngl::print(playerNr == 0 ? "Q : Shoot" : "- : Shoot", x, y + 74);
	jngl::print(playerNr == 0 ? "E : Gray Snake" : "# : Gray Snake", x, y + 116);
	jngl::print(playerNr == 0 ? "WSAD : Move" : "Arrow Keys : Move", x, y + 158);
}
