#include "Player.hpp"

#include "Control.hpp"
#include "engine/Paths.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"
#include "Snake.hpp"
#include "constants.hpp"
#include "Game.hpp" // FIXME: Sollte nicht notwendig sein
#include "Cursor.hpp"
#include "Shot.hpp"
#include "FieldIndex.hpp"

#include <algorithm>
#include <cmath>
#include <jngl.hpp>
#include <time.h>

Player::Player(const size_t playerNr, std::unique_ptr<Control> control) : playerNr(playerNr) {

	speed = 0.0;
	p0rotation = 0;
	p1rotation = 270;
	if (playerNr == 0) {
		position.x = -496;
		position.y = 0;
	}

	if (playerNr == 1) {
		position.x = 0;
		position.y = 496;
	}

	if (control) {
		this->control = std::move(control);
	} else {
		const auto controllers = jngl::getConnectedControllers();
		if (controllers.size() > playerNr) {
			this->control.reset(new Gamepad(controllers[playerNr], playerNr));
		} else {
			this->control.reset(new Keyboard(playerNr));
		}
	}
}

void Player::draw() const {
	jngl::pushMatrix();
	jngl::translate(position.x, position.y);
	jngl::rotate(playerNr == 0 ? p0rotation : p1rotation);
	jngl::draw(playerNr == 0 ? GetPaths().Graphics() + "pfeil_blau"
	                         : GetPaths().Graphics() + "pfeil_rot",
	           -35 - std::sin(waveStart * (1 + playerNr * 0.1)) * 1.8, -22);
	if (cooldown_counter > 0) {
		jngl::translate(-34, -15);
		jngl::setColor(30, 30, 30);
		jngl::drawRect(-2, -2, 12, 34);
		jngl::setColor(255, 255, 255);
		jngl::drawRect(0, 0, 8, 30);
		jngl::setColor(playerNr == 0 ? colorPlayerA : colorPlayerB);
		jngl::drawRect(0, 0, 8, 30 - cooldown_counter);
	}
	jngl::popMatrix();

	const int x = playerNr == 0 ? -900 : 600;
	const int y = -500;
	jngl::setFontSize(26);
	jngl::setFontColor(playerNr == 0 ? colorPlayerA : colorPlayerB);
	jngl::print("Player " + std::to_string(playerNr+1), x, y);
	jngl::setFontColor(0, 0, 0);
	if (!transmitting || int(waveStart) % 2 == 0) {
		jngl::print("Transmission: " + std::to_string(std::lround(progress)) + "%", x, y + 40);
	}

	/// Munition anzeigen
	jngl::setColor(255, 0, 0);
	for (int i = 0; i < lround(ammo); ++i) {
		jngl::draw(GetPaths().Graphics() + "rocket_red", x + i * 30 + 8, y + 95);
	}

	// Steuerung anzeigen
	control->draw_help(playerNr);
}

/// Sign function
template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

void Player::spawnCursorOr(const FieldIndex fieldPos, const FieldIndex direction,
                           const std::function<void()> callback) {
	const auto& field = GetGame().getField();
	if (field.getCell(fieldPos).isMine(playerNr)) {
		activeCursor[0] = new Cursor(fieldPos, direction, playerNr);
		addGameObject(activeCursor[0]);
	} else {
		callback();
	}
}

bool Player::has_won() {
	if (std::lround(progress) >= 100) return true;
	return false;
}

int Player::getPlayerNr() {
	return playerNr;
}

bool Player::isTransmitting() const {
	return transmitting;
}

void Player::step() {
	transmitting = false;
	if (playerNr == 0) {
		waveStart -= 0.1;
	} else {
		waveStart += 0.1;
	}

	ammo += 0.01;
	if (ammo > MAX_AMMO) { ammo = MAX_AMMO; }
	--cooldown_counter;
	const double acc = control->getMovement() * 0.6;
	speed += acc;
	if (sgn(acc) != sgn(speed)) { speed = 0; }
	const double TOP_SPEED = 8;
	speed = std::max(-TOP_SPEED, std::min(speed, TOP_SPEED));

	if (playerNr == 0) {
		position.y += speed;
		if (position.y > 496) {
			position.y = 496;
		} else if (position.y < -496) {
			position.y = -496;
		}
	} else {
		position.x += speed;
		if (position.x > 496) {
			position.x = 496;
		} else if (position.x < -496) {
			position.x = -496;
		}
	}

	if (control->getSwap()) {
		jngl::play(GetPaths().data() + "sfx/wusch.ogg");
		if (playerNr == 0) {
			if (position.x == -496) {
				position.x = 496;
				p0rotation += 180;
			} else if (position.x == 496) {
				position.x = -496;
				p0rotation -= 180;
			}
		} else {
			if (position.y == -496) {
				position.y = 496;
				p1rotation -= 180;
			} else if (position.y == 496) {
				position.y = -496;
				p1rotation += 180;
			}
		}
		// Man soll auf pro Seite einen Cursor starten können, daher activeCursor[0] austauschen:
		std::swap(activeCursor[0], activeCursor[1]);
	}

	const auto fieldPos = FieldIndex::fromScreenPos(getPosition());
	FieldIndex direction; // die Richtung in die wir zeigen/schießen
	if (playerNr == 0) {
		if (position.x < 0) {
			direction = DIRECTION_RIGHT;
		} else {
			direction = DIRECTION_LEFT;
		}
	} else {
		if (position.y < 0) {
			direction = DIRECTION_DOWN;
		} else {
			direction = DIRECTION_UP;
		}
	}
	const bool snakePressed = control->getSnake();
	const bool neutralSnakePressed = control->getNeutralSnake();
	auto& field = GetGame().getField();
	if (snakePressed || neutralSnakePressed) {
		const auto snakeType = neutralSnakePressed
		                           ? Cell::Type::NEUTRAL
		                           : (playerNr == 0 ? Cell::Type::PLAYER_A : Cell::Type::PLAYER_B);
		if (activeCursor[0]) { // Gibt es gerade ein Courser?
			const auto posAndDirection = activeCursor[0]->getSpawnPos(boost::none);
			addGameObject(new Snake(posAndDirection.first, posAndDirection.second, snakeType));
			GetGame().remove(activeCursor[0]);
			activeCursor[0] = nullptr;
		} else {
			if (cooldown_counter < 0) {
				cooldown_counter = 30; // Nur alle 30 Frames uns weiter setzen
				spawnCursorOr(
					fieldPos, direction, [this, fieldPos, direction, snakeType, &field]() {
						// Nur auf ein einem leeren Feld kann eine Schlange gestartet werden:
						if (field.getCell(fieldPos).getType() == Cell::Type::NOTHING) {
							addGameObject(new Snake(fieldPos, direction, snakeType));
						}
					}
				);
			}
		}
	}

	if (std::lround(ammo) > 0 && control->getShoot()) {
		if (activeCursor[0]) { // Gibt es gerade ein Courser?
			const auto posAndDirection = activeCursor[0]->getSpawnPos(playerNr);
			addGameObject(new Shot(posAndDirection.first, posAndDirection.second, playerNr));
			GetGame().remove(activeCursor[0]);
			activeCursor[0] = nullptr;
		} else {
			spawnCursorOr(fieldPos, direction, [this, fieldPos, direction]() {
				addGameObject(new Shot(fieldPos, direction, playerNr));
			});
		}
		ammo -= 1;
	}

	struct Checker {
		FieldIndex index;
		std::vector<FieldIndex> trail;
	};
	std::vector<Checker> checkers;
	std::vector<FieldIndex> visited;
	// Gucken ob eine Verbindung zwischen links nach rechts bzw. oben nach unten besteht:
	for (int i = 0; i < FIELD_SIZE; ++i) {
		FieldIndex index{ i, 0 };
		if (playerNr == 1) { index = FieldIndex{ 0, i }; }
		if (field.getCell(index).isMine(playerNr)) { checkers.emplace_back(Checker{ index, {} }); }
	}
	while (!checkers.empty()) {
		std::vector<Checker> newCheckers;
		for (const auto& checker : checkers) {
			FieldIndex toCheck[4] = {
				checker.index + DIRECTION_DOWN,
				checker.index + DIRECTION_UP,
				checker.index + DIRECTION_LEFT,
				checker.index + DIRECTION_RIGHT,
			};
			for (const auto& index : toCheck) {
				if (std::find(visited.begin(), visited.end(), index) == visited.end() &&
				    field.getCell(index).isMine(playerNr)) {
					if ((playerNr == 0 && index.j == FIELD_SIZE - 1) ||
					    (playerNr == 1 && index.i == FIELD_SIZE - 1)) {
						// Wir haben die andere Seite erreicht!
						double waveCounter = waveStart;
						for (const auto& index : checker.trail) {
							field.getCell(index).setActive(true, waveCounter);
							waveCounter += 0.1;
						}
						field.getCell(checker.index).setActive(true, waveCounter);
						field.getCell(index).setActive(true, waveCounter);
						progress += 0.04;
						transmitting = true;
						return;
					}
					visited.emplace_back(index);
					newCheckers.emplace_back(Checker{ index, checker.trail });
					newCheckers.back().trail.emplace_back(checker.index);
				}
			}
		}
		checkers = newCheckers;
	}
}
