#pragma once

#include "Cell.hpp"
#include "FieldIndex.hpp"
#include "constants.hpp"

#include <array>

class Field {
public:
	Field();
	Field(const Field&) = delete;
	Field(Field&&) = delete;
	Field& operator=(const Field&) = delete;
	Field& operator=(Field&&) = delete;

	Cell& getCell(FieldIndex index);
	const Cell& getCell(FieldIndex index) const;

	void step();
	void draw() const;

private:
	std::array<std::array<Cell, FIELD_SIZE>, FIELD_SIZE> cells;
	Cell borderCell;
};
