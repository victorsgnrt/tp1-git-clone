#include "Shot.hpp"

#include "FieldIndex.hpp"
#include "constants.hpp"
#include "Game.hpp"
#include "engine/Paths.hpp"

#include <jngl.hpp>

Shot::Shot(const FieldIndex fieldPos, const FieldIndex direction, const int playerNr)
: fieldPos(fieldPos - direction), direction(direction),
  sprite(GetPaths().Graphics() + "rocket_" + (playerNr == 0 ? "blue" : "green")),
  spriteRed(GetPaths().Graphics() + "rocket_red") {
	position = this->fieldPos.toScreenPos(); // Dies liegt außerhalb des Spielfelds
	targetPosition = fieldPos.toScreenPos(); // Und dies ist die richtige Position
	sprite.setCenter(0, 0);
	jngl::play(GetPaths().data() + "sfx/shot.ogg");
	if (direction == DIRECTION_DOWN) {
		spriteRotation = 180;
	}
	if (direction == DIRECTION_LEFT) {
		spriteRotation = 270;
	}
	if (direction == DIRECTION_RIGHT) {
		spriteRotation = 90;
	}
}

void Shot::step() {
	++spriteCounter;
	Vector2d animationDirection = (targetPosition - position);
	if (animationDirection.LengthSq() > 0) {
		animationDirection = animationDirection.Normalize();
	}
	const int FRAMES_PER_CELL = 4;
	position += animationDirection * (1.0 / FRAMES_PER_CELL) * CELL_SIZE;

	if (--counter > 0) {
		return;
	}
	counter = FRAMES_PER_CELL; // Nur alle FRAMES_PER_CELL Frames uns weiter setzen
	fieldPos += direction;
	targetPosition = fieldPos.toScreenPos();
	 auto& field = GetGame().getField();
	auto& cell = field.getCell(fieldPos);
	if (cell.getType() != Cell::Type::NOTHING) {
		destroy(fieldPos);
		jngl::play(GetPaths().data() + "sfx/explosion.ogg");
		GetGame().remove(this);
	}
}

void Shot::draw() const {
	jngl::setColor(230, 20, 20);
	jngl::pushMatrix();
	jngl::translate(position);
	jngl::rotate(spriteRotation);
	if (spriteCounter % 8 < 4) {
		spriteRed.draw();
	} else {
		sprite.draw();
	}
	jngl::popMatrix();
}

void Shot::destroy(FieldIndex fieldPos){
	auto& field = GetGame().getField();
	field.getCell(fieldPos).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_UP).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_UP+DIRECTION_LEFT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_UP+DIRECTION_RIGHT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_DOWN).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_DOWN+DIRECTION_LEFT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_DOWN+DIRECTION_RIGHT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_LEFT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_RIGHT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_UP+DIRECTION_UP).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_DOWN+DIRECTION_DOWN).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_LEFT+DIRECTION_LEFT).setType(Cell::Type::NOTHING);
	field.getCell(fieldPos+DIRECTION_RIGHT+DIRECTION_RIGHT).setType(Cell::Type::NOTHING);
}
