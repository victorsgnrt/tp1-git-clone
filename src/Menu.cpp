#include "Menu.hpp"

#include "engine/Fade.hpp"
#include "engine/Paths.hpp"
#include "OptionsMenu.hpp"
#include "Game.hpp"

Menu::Menu() {
	auto buttonBox = std::make_shared<ButtonBox>(0, 0);
	buttonBox->Add("Play",
	               []() { jngl::setWork(std::make_shared<Fade>(std::make_shared<Game>())); });
	buttonBox->Add("Options", []() {
		jngl::setWork(std::make_shared<Fade>(std::make_shared<OptionsMenu>()));
	});
	buttonBox->Add("Quit", jngl::quit);
	AddWidget(buttonBox);
}

void Menu::step() {
	StepWidgets();
}

void Menu::draw() const {
	DrawWidgets();
}
