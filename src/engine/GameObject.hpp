#pragma once

#include "Vector2d.hpp"

#include <list>
#include <memory>
#include <boost/optional.hpp>

class Game;
class Video;

class GameObject {
public:
	virtual ~GameObject() = default;
	virtual void step() = 0;
	virtual void draw() const = 0;
	virtual Vector2d getPosition() const;
	static void SetGame(Game*);
	double getZIndex() const;
	void overwriteZIndex(double);

protected:
	void addGameObject(GameObject*) const;

	Vector2d position;
	static Game& GetGame();

private:
	static Game* game_;
	boost::optional<double> zIndex;
};

class MoveableObject : public GameObject {
public:
	virtual void Accelerate(const Vector2d&);
	virtual Vector2d GetSpeed() const;
	virtual void Move(const Vector2d&);

protected:
	void StepMovement();
	Vector2d speed;
	Vector2d acc_;
};
