#pragma once

#include "Sprite.hpp"

#include <list>
#include <set>
#include <jngl.hpp>

class Widget;

class Work : public jngl::Work {
public:
	virtual void step() = 0;
	virtual void draw() const = 0;
	virtual ~Work() = default;
	void stepSprites();
	void drawSprites() const;
	void StepWidgets();
	void DrawWidgets() const;
	void AddWidget(std::shared_ptr<Widget>);
	void addSprite(Sprite*);
	void removeSprite(Sprite*);
	void FocusNext();
	void FocusPrevious();
protected:
	void StepFocus();
	std::vector<std::shared_ptr<Widget>> widgets_;
	std::list<Sprite*> sprites;
	std::set<Sprite*> needToRemove;
	std::shared_ptr<Widget> focusedWidget_;
};
