#include "Work.hpp"

#include "../gui/Widget.hpp"
#include "Screen.hpp"

#include <algorithm>
#include <sstream>

void Work::FocusPrevious() {
	focusedWidget_->SetFocus(false);
	if (focusedWidget_ == widgets_.front()) {
		focusedWidget_ = widgets_.back();
	} else {
		auto it = std::find(widgets_.begin(), widgets_.end(), focusedWidget_);
		--it;
		focusedWidget_ = *it;
	}
}

void Work::FocusNext() {
	focusedWidget_->SetFocus(false);
	if (focusedWidget_ == widgets_.back()) {
		focusedWidget_ = widgets_.front();
	} else {
		std::vector<std::shared_ptr<Widget>>::iterator it =
		    std::find(widgets_.begin(), widgets_.end(), focusedWidget_);
		++it;
		focusedWidget_ = *it;
	}
}

void Work::StepFocus() {
	if (widgets_.empty()) {
		return;
	}
	if (jngl::keyPressed(jngl::key::Down) || jngl::keyPressed(jngl::key::Tab)) {
		FocusNext();
	}
	if (!focusedWidget_->getSensitive()) {
		FocusNext();
	}
	if (jngl::keyPressed(jngl::key::Up)) {
		FocusPrevious();
		while (!focusedWidget_->getSensitive()) {
			FocusPrevious();
		}
	}
	focusedWidget_->SetFocus(true);
}

void Work::StepWidgets() {
	bool debug = true;
	if (!widgets_.empty()) {
		debug = widgets_.front()->getDebug();
	}
	auto end = widgets_.end();
	for (auto it = widgets_.begin(); it != end; ++it) {
		(*it)->step();
		if (jngl::keyPressed(jngl::key::F1)) {
			(*it)->setDebug(!debug);
		}
	}
	StepFocus();
	if (debug) {
		std::stringstream sstream;
		sstream << "Mouse: " << GetScreen().getMouseX() << " " << -GetScreen().getMouseY()
		        << " down: " << jngl::mouseDown() << std::endl;
		jngl::setTitle(sstream.str());
	}
}

void Work::DrawWidgets() const {
	auto end = widgets_.end();
	for (auto it = widgets_.begin(); it != end; ++it) {
		(*it)->draw();
	}
}

void Work::stepSprites() {
	for (auto& sprite : sprites) {
		sprite->step();
	}
	for (auto& sprite : needToRemove) {
		sprites.remove(sprite);
		delete sprite;
	}
	needToRemove.clear();
}

void Work::drawSprites() const {
	for (auto& sprite : sprites) {
		sprite->draw();
	}
}

void Work::addSprite(Sprite* sprite) {
	sprites.push_back(sprite);
}

void Work::removeSprite(Sprite* sprite) {
	needToRemove.insert(sprite);
}

void Work::AddWidget(std::shared_ptr<Widget> widget) {
	widgets_.push_back(widget);
	if (!focusedWidget_) {
		focusedWidget_ = widget;
	}
	widget->onAdd(*this);
	StepFocus();
}
