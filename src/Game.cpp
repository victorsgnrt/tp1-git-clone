#include "Game.hpp"

#include "GameOverScreen.hpp"
#include "engine/Paths.hpp"
#include "engine/Options.hpp"
#include "engine/Screen.hpp"
#include "engine/helper.hpp"
#include "PauseMenu.hpp"
#include "Player.hpp"
#include "Control.hpp"
#include "FieldIndex.hpp"

#include <string>
#include <cmath>

Game::Game() : players{{ Player(0), Player(1) }} {
	GameObject::SetGame(this);
	jngl::setBackgroundColor(215, 215, 215);

	jngl::setMouseVisible(false);
}

Game::~Game() {
	jngl::setMouseVisible(true);
}

void Game::step() {
	// Hier und nicht am Ende Objekte hinzufügen, damit diese mindest einmal gesteppt werden,
	// bevor sie gezeichnet werden.
	addObjects();

	for (auto& gameObject : gameObjects) {
		gameObject->step();
	}
	field.step(); // Vor player.step(), Cell::step() Cell::active zurücksetzt.
	bool transmitting = false;
	for (auto& player : players) {
		player.step();

		if (player.has_won())
		{
			jngl::setWork(std::make_shared<GameOverScreen>(jngl::getWork(), player.getPlayerNr()));
		}
		if (player.isTransmitting()) {
			transmitting = true;
		}
	}
	const static std::string filename = GetPaths().data() + "sfx/transmission.ogg";
	if (transmitting && !jngl::isPlaying(filename)) {
		jngl::play(filename);
	}
	if (!transmitting && jngl::isPlaying(filename)) {
		jngl::stop(filename);
	}

	const static std::string backgroundMusic = GetPaths().data() + "sfx/Azureflux_-_The_Prodigy_-_Wake_The_Fuck_Up__Azureflux_Remix_.ogg";
	if (!jngl::isPlaying(backgroundMusic)) {
		jngl::play(backgroundMusic);
	}

	if (jngl::keyPressed(jngl::key::Escape)) {
		onQuitEvent();
	}

	// Eventuell soll ein Objekt entfernt werden, welches nicht mehr gezeichnet werden darf.
	// Daher jetzt entfernen:
	removeObjects();
}

void Game::addObjects() {
	for (auto& gameObject : needToAdd) {
		gameObjects.push_back(std::move(gameObject));
	}
	needToAdd.clear();
}

void Game::removeObjects() {
	for (const GameObject* const toRemove : needToRemove) {
		for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
			if (it->get() == toRemove) {
				gameObjects.erase(it);
				break;
			}
		}
	}
	needToRemove.clear();
}

void Game::draw() const {
	jngl::setColor(255, 255, 255);
	jngl::drawRect(-496, -496, 992, 992);
	field.draw();
	jngl::setColor(0, 0, 0);
	jngl::drawLine(-496,  496,  496,  496);
	jngl::drawLine( 496,  496,  496, -496);
	jngl::drawLine( 496, -496, -496, -496);
	jngl::drawLine(-496, -496, -496,  496);

	for (auto& gameObject : gameObjects) {
		gameObject->draw();
	}

	for (auto& player : players) {
		player.draw();
	}
}

void Game::add(GameObject* const gameObject) {
	needToAdd.emplace_back(gameObject);
}

void Game::remove(GameObject* const object) {
	needToRemove.push_back(object);
}

Field& Game::getField() {
	return field;
}

void Game::onQuitEvent() {
	jngl::setWork(std::make_shared<PauseMenu>(jngl::getWork()));
}
